import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import {Observable} from 'rxjs';
import {map, tap, filter} from 'rxjs/operators';
import {User, IUserResponse} from './user/user.class'


@Injectable()
export class AppService{

iuserResponse: IUserResponse;


    constructor(private http: HttpClient){}

    private userURL = 'https://api.myjson.com/bins/o0vh0';
    private oldurl = '/api/users';

    search(filter: {name: string} = {name: ''}, page = 1): 
    Observable <IUserResponse> {
        return this.http.get<IUserResponse>('/api/users')
        .pipe(
          tap((response: IUserResponse) => {
              console.log(response),
              response.results = response.results
              .map(user => new User(user.id, user.name))
              // Not filtering in the server since in-memory-web-api has somewhat restricted api
              .filter(user => user.name.includes(filter.name))
    
            return response;
          })
          );
      }

     ngOnInit(){
        
     } 

     getRespose(){
          
        let users = [
            { id: "mjumarang@transitions.com", name: 'Jumarang, Mark' },
            { id: "coren.silva@essilor.com", name: 'Silva, Coren' },
            { id: "Jalliza.gomez@essilor.com", name: 'Gomez, Jalliza Eiva' }
            
          ];
          return {users: {
            total: users.length,
            results: users
          }};
     }
}