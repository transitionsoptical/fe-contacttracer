export class Hero {

  constructor(
    public id?: number,
    public name?: string,
    public power?: string,
    public alterEgo?: string,
    public email?: string,
    public incidentType?:string,
    public employeeTypes?:string) {
    
  }

}
