import { OnInit, Component, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "../services/auth.service";
import { itemService } from '../items/item.service';
import { ApproverDetail, IItem } from "../items/item";
import { ModalComponent } from "../shared/modal/modal.component";

@Component({
    selector: 'approval-form',
    templateUrl: './approval.component.html'
    
  })

export class ApprovalComponent implements OnInit {

    heroForm: FormGroup;
    approvalDetail: ApproverDetail[] = [];
    approvedRequest: boolean = false;
    submitApproval: boolean = false;
    validApproval : boolean = false;
    showInformation: boolean = false;
    information: string;
    reqInfo: IItem = undefined;
    constructor( 
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private fb: FormBuilder, 
        private authService: AuthService,
        private itemService: itemService) { 
         
        }

    @ViewChild(ModalComponent) child;


    ngOnInit(){

        const param = this.route.snapshot.paramMap.get('id');
        const paramType = this.route.snapshot.paramMap.get('type');
            if (param) {
                const id = +param;
                const type = paramType;
                this.heroForm = this.formBuilder.group({
                    id : [id],
                    status: ['Yes'],
                    comment:[''],
                    type: type
                });
              
                this.getApprovalStatus(type.toString(),id.toString())
            } 

    }


    getApprovalStatus(type: string, id: string): void{
        this.child.showLoading = true;
        this.itemService.getItem(Number(id)).subscribe(
            item => {
              console.log(item);
              this.reqInfo = item;
              
              if (type == "finance-manager")
              {
                  if (item.tosFinanceManagerStatus == "Approved" && item.departmentManagerStatus != null)
                  {
                    this.information = "This was already approved";
                    this.showInformation = true;
                  }
              }
              else if (type == "itmanager")
              {
                  if (item.ITManagerStatus == "Approved" && item.ITManagerStatus != null)
                  {
                    this.information = "This was already approved";
                    this.showInformation = true;
                  }
              }
              this.child.showLoading = false;
            },
            error => {
              
            });
    }


    OnSubmit():void{
        this.showInformation = true;
        this.information = "You have successfully submitted the request";
        this.itemService.setApprovalStatus(this.heroForm.value.id,this.heroForm.value); 
    }

    onBack(): void{
        this.router.navigate(['/view'])
    }
}
