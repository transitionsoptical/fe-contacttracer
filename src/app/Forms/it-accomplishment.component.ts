import { OnInit, Component, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "../services/auth.service";
import { itemService } from '../items/item.service';
import { ModalComponent } from "../shared/modal/modal.component";



@Component({
    selector: 'it-accomplishment',
    templateUrl: './it-accomplishment.component.html'
  })
  
  
  
  export class ITAccomplistmentComponent implements OnInit {

    heroForm: FormGroup;
    isAlreadyAccomplish: boolean= false;
    displayAccomplishby: boolean=false;
    @ViewChild(ModalComponent) childx;
  
    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private fb: FormBuilder,
        private authService: AuthService,
        private itemService: itemService) {
        
    }
   

    ngOnInit(): void {
       
        let _id = Number(this.route.snapshot.paramMap.get('id'));
        this.heroForm = this.formBuilder.group({
            id: _id,
            dateStarted: ['',Validators.required],
            dateFinished: ['',Validators.required],
            actionTaken: ['',Validators.required],
            accomplishmentByName:[this.authService.name],
            accomplishmentByEmail:['']
        });

        this.getItem(_id);
    }

  

      onOk() {
        this.childx.showLoading = true;
        this.heroForm.value["accomplishmentByName"] = this.authService.name;
        this.heroForm.value["accomplishmentByEmail"] = this.authService.email;
        // this.itemService.setAccomplishment(this.heroForm.value);  
        this.childx.popupMessage = 'Successfully Saved!'
        this.childx.redirectUrl = ['/view'];
        this.childx.showMessage = true;
      }    

    private getItem(id: number) {
        this.itemService.getItem(id).subscribe(
          item => {
            console.log(item);
                if (item.actionTaken != null){
                    this.heroForm.patchValue(item);
                    this.isAlreadyAccomplish = true;
                    this.displayAccomplishby = true;
                }
                   

          })
    }

    onCancel() {
        //this.child.showLoading = true;
        this.router.navigateByUrl('/view');
      }
      
  
  }  