import { OnInit, Component, ViewChild, AfterViewInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "../services/auth.service";
import { itemService } from '../items/item.service';
import { Guid } from "guid-typescript";
import { iApprovers, DocumentChangeNotice, ApproverDetail, AttachementDetails, Attendance } from "../items/item";
import { Observable, of } from "rxjs";
import { IUser } from "../items/User";
import { startWith, map } from "rxjs/operators";
import references from '../../assets/references.json';
import { ModalComponent } from "../shared/modal/modal.component";


@Component({
  selector: 'disp-form',
  templateUrl: './disp.component.html'
})


export class DispComponent implements OnInit {


  

  public id: string;
  param: number = 0;
  paramIsprocess: boolean = false;
  heroForm: FormGroup;
  currentStringDate: string;
  filteredEmpName: Observable<IUser[]>;
  userEmpList: any[] = [];
  userSupList: any[] = [];
 
  requestTypeList: any[] = [];
  staffCategoryList: any[] = [];
  employmentStatusList: any[] = [];
  softwareInstalltionList: any[] = [];
  systemDevelopmentList: any[] = [];
  systemAccessRequestList: any[] = [];
  accessTypeList: any[] = [];
  filteredSupName: Observable<IUser[]>;

  errorMessage = '';


  fileList: File[] = [];
  listOfFiles: any[] = [];
  listOfFilesSaved: AttachementDetails[] = [];
  listOfFilesSavedCompleted: AttachementDetails[] = [];
  withAttachment = false;
  attachmentValidation = false;


  attachementDetails: AttachementDetails;
  attachementDetailsList : any[] = [];
  newAttachment: boolean= false;

  withattachedFile: boolean = false;

  @ViewChild('attachments') attachment: any;
  
  attachmentRef: string;

  countryList: any[] = [];
  creditTermList: any[] = [];
  bankList: any[] = [];

  showInfoA: boolean=false;
  showInfoB: boolean=false;
  showInfoC: boolean=false;
  showInfoD: boolean=false;

  isAdmin = false;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private itemService: itemService) {
      this.id = Guid.create().toString();

      this.itemService.getCountries().subscribe(
        res => {
          this.countryList = res;
       });
  
       this.isAdmin = this.authService.isAdmin;
       this.itemService.getBankInfo().subscribe(
        res => {
          this.bankList = res;
       });
  
  }
  @ViewChild(ModalComponent) child;

  ngAfterViewInit() {
    setTimeout(() => {
      this.child.popupMessage = 'tessss';
      this.child.redirectUrl = '/view';
    });
  }

 

  ngOnInit() {
    
    this.currentStringDate = new Date().toISOString().substring(0, 10);
    
    this.heroForm = this.formBuilder.group({
      id: [0],
      added: [this.currentStringDate],
      requestor: [this.authService.name, Validators.required],
      requestorEmail: [this.authService.email],
      requestType: ['', Validators.required],
      entity: ['', Validators.required],
      supplierType: ['', Validators.required],
      name: ['', Validators.required],
      bankAccountName: [''],
      attention: [''],
      address1: ['', Validators.required],
      address2: [''],
      address3: [''],
      city: ['', Validators.required],
      post: [''],
      state: [''],
      country: ['', Validators.required],
      countryCode: [''],
      telephone1: [''],
      telephone2: ['', Validators.required],
      faxTelex1: [''],
      mobileNumber: [''],
      natureOfTransaction: ['', Validators.required],
      creditTerm: ['', Validators.required],
      creditTermCode: [''],
      crTerms: [''],
      purAcct: ['668'],
      sub: [''],
      cc: [''],
      apAccount: [''],
      apSub: [''],
      apCC: [''],
      taxable: ['' , Validators.required],
      taxZone: [''],
      taxClass: [''],
      taxUsage: [''],
      bankName: [''],
      bankCode: [''],
      branch: [''],
      currency: ['', Validators.required],
      bankAccount: [''],
      emailAddress1: [''],
      emailAddress2: [''],
      emailAddress3: [''],
      attachmentRef: [''],
      status: ['Pending'],
      pssPersonnelAssigned : [''],
      pssPersonnelDate: [''],
      pssPersonnelStatus:[''],
      requestNo:[''],
      submit:['false'],
      financeManagerAssigned: [''],
      tosFinanceManagerStatus: [''],
      tosFinanceManagerDate: [''],
      fssAPTL:[''],
      fssAPTLStatus:[''],
      fssAPTLDateApproval:[''],
      apDirectorAssigned: [''],
      apDirectorDate:[''],
      apDirectorStatus:[''],
      supplier:[''],
    });


    this.param = Number(this.route.snapshot.paramMap.get('id'));
    let process = this.route.snapshot.paramMap.get('process');
    if (this.param) {
      const id = this.param;
      this.getItem(id);
      if (process != undefined)
        this.paramIsprocess = true;
    }
    else {
      this.param = 0;
    }
    //console.log(this.heroForm.controls['financeTEAuditExceptionsDetails'].value); 
    this.child.showLoading = true;
    this.itemService.getEmployee().subscribe(
      res => {
        this.userEmpList = res;
        this.userSupList = res;
        this.child.showLoading = false;
      });
    //console.log(references);


    let currentYear = (new Date()).getFullYear();

    
  }
 
  private _filterUser(value: string, userVar: any[]): IUser[] {
    const filterValue = value != undefined ? value.toLowerCase() : '';
    return userVar.filter((item: IUser) =>
      item.name.toLocaleLowerCase().indexOf(filterValue) !== -1);
  }

  private getItem(id: number) {
    this.child.showLoading = true;
    this.itemService.getItem(id).subscribe(
      item => {
        console.log(item);
        this.heroForm.patchValue(item);
       
        this.itemService.getCreditTerm().subscribe(
          res => {
            this.creditTermList = res.filter(x => x.entity ==item.entity);
         });


        if (this.bankList.length> 0){
            let sbankName =  this.bankList.filter(x => x.bankCode == item.bankName)
            if (sbankName.length > 0){
              this.heroForm.patchValue({
                bankName: sbankName[0].bankName,
                bankCode: sbankName[0].bankCode
              })        
            }
        }
        this.itemService.getAttachmentDetails(item.attachmentRef).subscribe(
          res =>{
              this.listOfFilesSavedCompleted = res.filter(x1 => x1.additionalInfo.includes('Completed'));
              this.listOfFilesSaved = res.filter(x1 => x1.additionalInfo.includes('Primary'));
              if (this.listOfFilesSaved.length)
                this.withAttachment = true;
          }
        )

        this.child.showLoading = false;
      },
      error => {
        this.child.popupMessage = <any>error;
        this.child.showMessage = true;
      });
  }
 

  setEmpDetails(user) {
    this.heroForm.controls['requisitionerEmail'].setValue(user.email);
  }
  setSupDetails(user) {
    this.heroForm.controls['immediateSupervisorEmail'].setValue(user.email);
  }
  onCancel() {
    //this.child.showLoading = true;
    this.router.navigateByUrl('/view');
  }
  onOk(submit: string) {
    if (submit=="true"){
      this.heroForm.value.submit = "true";
      this.child.popupMessage = 'Successfully Submit for approval!'
    }
    else{
       this.heroForm.value.submit =   "false";
       this.child.popupMessage = 'Successfully Saved!'
    }

    if (this.paramIsprocess){
      this.child.popupMessage = 'Processing Saved!';
      this.heroForm.value.pssPersonnelAssigned = this.authService.name;
      this.heroForm.value.pssPersonnelDate= this.currentStringDate;
      this.heroForm.value.pssPersonnelStatus="Processed";
      this.heroForm.value.status="Processed";
    }

    this.heroForm.value.bankName = this.heroForm.value.bankCode;
    if (this.param == 0) {
      this.heroForm.value.attachmentRef = this.id.toString();
      this.itemService.addItem(this.heroForm.value).subscribe(res2 => {
        this.attachmentMethod(this.id.toString());
        console.log(res2);
        this.child.showLoading = false;
        
        this.child.redirectUrl = ['/view'];
        this.child.showMessage = true;
      },
        err => {
          this.child.showLoading = false;
          this.child.redirectUrl = '';
          this.child.popupMessage = 'Error Occurred. Please try again.';
          this.child.showMessage = true;
        });
    }
    else {

      if (this.paramIsprocess = true)
      {

      }
      this.itemService.editItem(this.param, this.heroForm.value).subscribe(res2 => {
        this.attachmentMethod(this.heroForm.value.attachmentRef);
        this.child.showLoading = false;
        this.child.redirectUrl = ['/view'];
        this.child.showMessage = true;
      },
        err => {
          this.child.showLoading = false;
          this.child.redirectUrl = '';
          this.child.popupMessage = 'Error Occurred. Please try again.';
          this.child.showMessage = true;
        });
    }
  }
  onDelete(id) {
    // if (confirm("Are you sure to delete this ITRF?")) {
    //   this.itemService.DeleteItem(this.param).subscribe(res2 => {
    //     this.child.showLoading = false;
    //     this.child.popupMessage = 'Successfully Deleted!'
    //     this.child.redirectUrl = ['/view'];
    //     this.child.showMessage = true;
    //   },
    //     err => {
    //       this.child.showLoading = false;
    //       this.child.redirectUrl = '';
    //       this.child.popupMessage = 'Error Occurred. Please try again.';
    //       this.child.showMessage = true;
    //     });
    // }
  }


  onFileChange(event: any){
   
    for (var i = 0; i <= event.target.files.length - 1; i++) {
      var selectedFile = event.target.files[i];
      this.fileList.push(selectedFile);
      this.listOfFiles.push(selectedFile.name)
     
    }

    this.attachment.nativeElement.value = '';

  }


   removeSelectedFile(index) {
    // Delete the item from fileNames list
    this.listOfFiles.splice(index, 1);
    // delete file from FileList
    this.fileList.splice(index, 1);
   }

   deleteAttachedFile(index,filename: string) {
    // Delete the item from fileNames list
    if(confirm("Are you sure to delete this file? This file will directly remove from the server.")) {
      let attach = this.attachementDetailsList.find(x => x.filename === filename);
      this.attachementDetailsList.splice(index, 1);
      this.itemService.DeleteAttachmentDetails(attach.id);
      this.itemService.DeleteAttachmenfile(filename,attach.id.toString());
    }
  }

  getEntityInfo(ev) : void{

    this.itemService.getCreditTerm().subscribe(
      res => {
        this.creditTermList = res.filter(x => x.entity ==ev.target.value);
     });

  }

  getCreditTermCode(ev): void{
    let crTerms = this.creditTermList.filter(x=> x.description == ev.target.value)
    if (crTerms.length > 0)
    {
      this.heroForm.patchValue({
        crTerms: crTerms[0].terms_code
      })
    } 
  }


  getCountryCode(ev): void{
    let countryCode = this.countryList.filter(x=> x.country_name == ev.target.value)
    if (countryCode.length > 0)
    {
      this.heroForm.patchValue({
        countryCode: countryCode[0].country_code
      })
    } 
  }


  getBankCode(ev): void{
    let bankCode = this.bankList.filter(x=> x.bankName == ev.target.value)
    if (bankCode.length > 0)
    {
      this.heroForm.patchValue({
        bankCode: bankCode[0].bankCode
      })
    } 
  }
  
  getTaxableInfo(ev): void{
    if ( ev.target.value == "Yes")
    {
      this.heroForm.patchValue({
        taxZone: 'AUS',
        taxClass: 'GST - 10%',
        taxUsage: 'GST'
      })
    }else
    {
      this.heroForm.patchValue({
        taxZone: 'AUS',
        taxClass: '00 - NON-TAXABLE',
        taxUsage: '00 - NON-TAXABLE'
      }); 
    }
  }


  attachmentMethod(refID: string ): void{
    const formData = new FormData();
    for (var i = 0; i <= this.fileList.length - 1; i++) {
       this.newAttachment = true;
       var selectedFile =this.fileList[i];
       formData.append('Files', selectedFile);
       formData.append('requestID', refID.toString());
     
       this.attachementDetails = new AttachementDetails(
         0,
         selectedFile.name,
         refID.toString(),
         "Completed"
         )
 
       this.attachementDetails.filename = selectedFile.name;
       this.itemService.addAttachmentsDetails(this.attachementDetails)
     }

      if (this.newAttachment)
       this.itemService.uploadAttachmentFile(formData);
  }


  deleteAttachedFileSaved(index,filename: string) {
    // Delete the item from fileNames list
    if(confirm("Are you sure to delete this file? This file will directly remove from the server.")) {
      let attach = this.listOfFilesSaved.find(x => x.filename === filename);
      this.listOfFilesSaved.splice(index, 1);
      this.itemService.DeleteAttachmentDetails(attach.id.toString());
      this.itemService.DeleteAttachmenfile(filename,attach.id.toString());
      this.withAttachment=false;
    }
  }

  
  downloadAttachment(_filename: string,_folderID: string):void{
    this.itemService.downloadAttachment(_filename,_folderID)
      .subscribe(x => {
          // It is necessary to create a new blob object with mime-type explicitly set
          // otherwise only Chrome works like it should
          var newBlob = new Blob([x], { type: 'application/octet-stream' });

          // IE doesn't allow using a blob object directly as link href
          // instead it is necessary to use msSaveOrOpenBlob
          if (window.navigator && window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveOrOpenBlob(newBlob);
              return;
          }

          // For other browsers: 
          // Create a link pointing to the ObjectURL containing the blob.
          const data = window.URL.createObjectURL(newBlob);

          var link = document.createElement('a');
          link.href = data;
          link.href = data;
          link.download = _filename;
          // this is necessary as link.click() does not work on the latest firefox
          link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

        });
  }

  toggleA(event: any){
    console.log(event.target.checked)
    if (event.target.checked)
     this.showInfoA = true;
    else
     this.showInfoA = false;
   }

   toggleB(event: any){
    console.log(event.target.checked)
    if (event.target.checked)
     this.showInfoB = true;
    else
     this.showInfoB = false;
   }

   toggleC(event: any){
    console.log(event.target.checked)
    if (event.target.checked)
     this.showInfoC = true;
    else
     this.showInfoC = false;
   }

   toggleD(event: any){
    console.log(event.target.checked)
    if (event.target.checked)
     this.showInfoD= true;
    else
     this.showInfoD = false;
   }

   onUpdateSupplier(): void{
  
  
    this.itemService.setSupplierCode(this.heroForm.value); 
    this.attachmentMethod(this.heroForm.value.attachmentRef);
    this.child.popupMessage = 'Updating Complete!'
    this.child.showMessage = true;
   }

   update(){
    this.router.navigateByUrl('/entry/' + this.heroForm.value.id);
   }


}
