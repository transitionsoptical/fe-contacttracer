import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';


@Component({
    selector: 'starter-template-form',
    templateUrl: './starterTemplateForm.component.html'
})

export class StarterTemplateFormComponent implements OnInit {
    
    constructor(public authService: AuthService
    ){ 

    }

    ngOnInit() {
    }
}