import { Guid } from "guid-typescript";

export interface IItem {
	id: number,
	requestNo: string,
	date: string,
	requisitionerName: string,
	requisitionerEmail: string,
	dateNeeded: string,
	employeeNames: string,
	department: string,
	section: string,
	immediateSupervisorName: string,
	immediateSupervisorEmail: string,
	requestType: string,
	positionTitle: string,
	employmentStatus: string,
	addtoAllPhilippinesGroup: boolean,
	staffCategory: string,
	addtoTOPIPTSGroup: boolean,
	addtoTOPIMAINPTSGroup: boolean,
	groupEmailAccount: string,
	purpose: string,
	softwareInstallation: boolean,
	softwareInstallationvalue: string,
	softwareApplication: string,
	systemDevelopment: boolean,
	systemDevelopmentValue: string,
	moduleNames: string,
	systemAccessRequest: boolean,
	systemAccessRequestValue: string,
	networkFolderName: string,
	accessType: string,
	mfgAccount: boolean,
	menuNumber: string,
	details: string,
	itApproverApprovalComment: string,

	departmentManagerStatus : string,
	departmentManagerComments: string,
	ITManagerStatus: string,
	ITManagerComments : string,

	actionTaken: string,
	accomplishmentByName: string,
	accomplishmentByEmail: string,
	dateStarted: string,
	dateFinished: string,
	status: string,
	attachmentRef: string,
	bankName: string,
	entity: string,
	tosFinanceManagerStatus: string



}

export interface IITAccomplishment {
	id: number,
	dateStarted: string,
	dateFinished: string,
	actionTaken: string,
	accomplishmentByName : string,
	accomplishmentByEmail : string
}



export interface AttachmentInfo {
	Files: FileList;
	requestID: string;
}


export interface IAttachementDetails {
	filename: string;
	itemID: string;
}

export class AttachementDetails {

	constructor(
		public id?: number,
		public filename?: string,
		public itemID?: string,
		public additionalInfo?: string
	) { }

}

export interface iApprovers {
	id: number;
	name: string;
	department: string;
	status: string;
	type: string;
}

export class ApproverDetail {
	constructor(
		public name?: string,
		public department?: string,
		public status?: string,
		public type?: string,
		public refID?: string
	) { }
}

export class DocumentChangeNotice {
	constructor(
		public ID?: number,
		public dateSubmit?: string,
		public department?: string,
		public refMOCNo?: string,
		public typeofChange?: string,
		public documentNo?: string,
		public documentTitle?: string,
		public docRevisionFrom?: string,
		public docRevisionTo?: string,
		public description?: string,
		public reason?: string,
		public distribution?: string,
		public noOfCopies?: string,
		public attachmentRef?: string,
		public submittedByName?: string,
		public submittedByEmail?: string,
		public submitted?: string,
		public status?: string

	) {
	}
}

export class Attendance {
	constructor(
		public refID?: string,
		public communicationDate1?: string,
		public communicationDate2?: string,
	) { }
}