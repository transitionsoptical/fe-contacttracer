export interface IUser
{
    id: number;
    name: string;
    username: string;
    email: string;
    address: string;
    phone: string;
    website: string;
}

export interface IUser1
{
    id: number;
    name: string;
    username: string;
    email: string;
    address: string;
    phone: string;
    website: string;
}


export interface ILoginUser{
    uid: string;
    email: string;
    displayName: string;
    photoURL: string;
    emailVerified: boolean;
}

export interface IUserApplication{
    id: string;
    email: string;
    isAdmin: number;
}