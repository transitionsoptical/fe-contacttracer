import { Injectable } from "@angular/core";
import { IItem, AttachmentInfo, IAttachementDetails, AttachementDetails, DocumentChangeNotice, iApprovers, ApproverDetail, Attendance } from "./item";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, tap, map, share } from "rxjs/operators";
import { IncidentInvestigation } from "./incidentInvestigation";
import { Http, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { IUserApplication } from "./User";
import { environment } from "../../environments/environment";


@Injectable({
  providedIn: 'root'
})

export class itemService {
  // 'https://api.myjson.com/bins/qjte4'
  ///api/items
  //
  //http://insightcore.transitions.com
  //https://insightcore.transitions.com/tos-vendor/_api/
  private employeeUrl = environment.mainAPI +  '/EmployeeMasters/';
  // private departmentUrl = 'https://insight-core.transitions.com/itrf/api/itrfdepartments';
  // private classificationsUrl = 'https://insight-core.transitions.com/act/api/financeactclassifications/';
  
  
  // private deleteUrl = 'https://insight-core.transitions.com/itrf/api/itrequestforms/'; 
  // private urlsetAccomplishment = "https://insight-core.transitions.com/itrf/api/setAccomplishment/";
   
  private attachmentUrl = environment.mainAPI +  '/UploadFile';
  private attachmentDownload = environment.mainAPI + '/download';
  private attachmentFileDelete= environment.mainAPI + '/deleteFile';


  private attachmentDetails =  environment.mainAPI + '/AttachmentsUpload/';
  private attachmentDetailsFilter =  environment.mainAPI + '/attachmentsFilter/';
  private attachmentDetailsdelete = environment.mainAPI + '/api/deleteattachment/';
  

  private applicationPerUsers = environment.mainAPI +  'GetApplicationPerUsers?ApplicationId=' + environment.appID;

  private putUrl = environment.vendorAPI +  '/VendorFormMains/';
  private itemUrl =  environment.vendorAPI +  '/VendorFormMains/';
  private urlCountries =  environment.vendorAPI + '/VendorFormTOSVDCountryCodes/';
  private urlCreditTerm =  environment.vendorAPI + '/VendorFormCreditTerms/';
  private urlBankInfo =  environment.vendorAPI + '/VendorFormBankInformations/';
  private urlsetApprovalStatus =  environment.vendorAPI + '/setApprovalStatus/';
  private urlsetSupplierCode =  environment.vendorAPI + '/setSupplierCode/';
  
  private urlsetsetProcess =   environment.vendorAPI + '/setProcess/';
 
  headers = new Headers({
    'accept': 'application/json'
  });

  _attachmentInfo: AttachmentInfo;

  public imageListCache = [];
  itemID: string;

  constructor(private http: HttpClient,
    private http2: Http,
    private router: Router) {
  }

  // getDepartment(): Observable<any[]> {
  //   return this.http.get<any[]>(this.departmentUrl).pipe(
  //     //tap(data => console.log('All: ' + JSON.stringify(data))),
  //     catchError(this.handleError)
  //   );
  // }

  // getClassification(): Observable<any[]> {
  //   return this.http.get<any[]>(this.classificationsUrl).pipe(
  //     //tap(data => console.log('All: ' + JSON.stringify(data))),
  //     catchError(this.handleError)
  //   );
  // }

  getItems(): Observable<IItem[]> {
    return this.http.get<IItem[]>(this.itemUrl).pipe(
      //tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  getItem(id: number): Observable<IItem | undefined> {
    return this.getItems().pipe(
      map((items: IItem[]) => items.find(p => p.id === id))
    );
  }

  getEmployee(): Observable<any[]> {
    return this.http.get<any[]>(this.employeeUrl).pipe(
      //tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  getAttachmentDetails(attachmentRef: string):Observable< AttachementDetails[]>{
    return this.http.get<AttachementDetails []>(this.attachmentDetailsFilter + "?itemID=" + attachmentRef).pipe(
      tap(data => console.log('All: '+ JSON.stringify(data))),
      catchError(this.handleError)
    );
  }  

  DeleteAttachmentDetails(id: string) {
    this.http2.delete(this.attachmentDetailsdelete + id).subscribe();
  }


  addItem(value: object): Observable<object> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    return this.http2.post(this.itemUrl, JSON.stringify(value), { headers: headers })

  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage = ''
    if (err.error instanceof ErrorEvent) {
      errorMessage = "An error occured:  ${err.error.message}";
    } else {
      errorMessage = "Server returned code: $(err.status}, error message is: ${err.message}"
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }



  getApplicationUsers(): Observable<IUserApplication[]> {
    return this.http.get<IUserApplication[]>(this.applicationPerUsers).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  getApplicationUsersLogin(_email: string): Observable<IUserApplication | undefined> {
    return this.getApplicationUsers().pipe(
      map((items: IUserApplication[]) => items.find(p => p.email === _email))
    );
  }


  getApplicationUsersAll(): Observable<IUserApplication[] | undefined> {
    return this.http.get<IUserApplication[]>(this.applicationPerUsers).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    )
  }

  addAttachmentsDetails(value: AttachementDetails) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    this.http2.post(this.attachmentDetails, JSON.stringify(value), { headers: headers })
      .subscribe(res => {
        console.log(res);
      },
        err => {
          console.log("Error occured");
        }
      );
  }

  

  editItem(id: number, value: object): Observable<object> {
    let headers = new Headers();

    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    console.log(options);
    console.log(JSON.stringify(value));
    return this.http2.put(this.putUrl + id, JSON.stringify(value), { headers: headers }
    )

  }

  // DeleteItem(id: number): Observable<object>{
  //   return this.http2.delete(this.deleteUrl + id);
  // }

  setApprovalStatus(id: number, value: ApproverDetail){
    let headers = new Headers();
    
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    
    console.log(options);
    console.log(JSON.stringify(value));
    this.http2.post(this.urlsetApprovalStatus, JSON.stringify(value) , {headers:headers}
    )
    .subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.log("Error occured");
      }
    );
  }


  setSupplierCode(value: object){
    let headers = new Headers();
    
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    
    console.log(options);
    console.log(JSON.stringify(value));
    this.http2.post(this.urlsetSupplierCode, JSON.stringify(value) , {headers:headers}
    )
    .subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.log("Error occured");
      }
    );
  }

  setProcess(value: object){
    let headers = new Headers();
    
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    
    console.log(options);
    console.log(JSON.stringify(value));
    this.http2.post(this.urlsetsetProcess, JSON.stringify(value) , {headers:headers}
    )
    .subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.log("Error occured");
      }
    );
  }



  // setAccomplishment(value: Object){
  //   let headers = new Headers();
    
  //   headers.append('Content-Type', 'application/json');
  //   let options = new RequestOptions({ headers: headers });
    
  //   console.log(options);
  //   console.log(JSON.stringify(value));
  //   this.http2.post(this.urlsetAccomplishment, JSON.stringify(value) , {headers:headers}
  //   )
  //   .subscribe(
  //     res => {
  //       console.log(res);
  //     },
  //     err => {
  //       console.log("Error occured");
  //     }
  //   );
  // }


  //Vendor Form
  getCountries(): Observable<any[]> {
    return this.http.get<any[]>(this.urlCountries).pipe(
      //tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  } 


  getCreditTerm(): Observable<any[]> {
    return this.http.get<any[]>(this.urlCreditTerm).pipe(
      //tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  } 

  getBankInfo(): Observable<any[]> {
    return this.http.get<any[]>(this.urlBankInfo).pipe(
      //tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  } 


  public downloadAttachment(_fileName: string, _folderID: string): Observable<Blob> {   
    //const options = { responseType: 'blob' }; there is no use of this
        // this.http refers to HttpClient. Note here that you cannot use the generic get<Blob> as it does not compile: instead you "choose" the appropriate API in this way.
        return this.http.get(this.attachmentDownload + "?filename=" + _fileName + "&folderID=" + _folderID, { responseType: 'blob' });
  }

  DeleteAttachmenfile(_filename: string, _filepath: string){
    this.http2.delete(this.attachmentFileDelete + "?filename=" + _filename + "&filepath=" + _filepath).subscribe();
  }

  
  uploadAttachmentFile(attachmentInfo: FormData){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    this.http2.post(this.attachmentUrl,attachmentInfo)
    .subscribe(
      res => {
        //console.log(res);
      
      },
      err => {
        console.log("Error occured");
      }
    );
  }

}