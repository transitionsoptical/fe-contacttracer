import { Injectable, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable, animationFrameScheduler } from 'rxjs';
import { itemService } from '../items/item.service';
import { ILoginUser, IUserApplication } from '../items/User';

@Injectable({
  providedIn: 'root'
})

export class AuthService implements OnInit {
  private user: Observable<firebase.User>;
  private userDetails: firebase.User = null;
  userData: any; // Save logged in user data
  public name: string = "";
  public email: string = "";
  public displayImage: string = "";
  public isAdmin: boolean = false;
  return: string = '';
  loginUserInfo: IUserApplication[] | undefined;
  preLogin: boolean = false;

  constructor(public _firebaseAuth: AngularFireAuth,
    public router: Router,
    private route: ActivatedRoute,
    private itemService: itemService) {

    this.user = _firebaseAuth.authState;

    this.itemService.getApplicationUsersAll().subscribe(
      loginUserInfo => {
        this.loginUserInfo = loginUserInfo;

        this.user.subscribe(
          (user) => {
            if (user) {
              this.userDetails = user;
              console.log(this.userDetails);
              console.log(this.loginUserInfo);
              this.name = user.displayName;
              this.email = user.email;
              this.displayImage = user.photoURL;

              if (this.preLogin) {
                let verifiedUser = this.loginUserInfo.find(x => x.email === user.email);

                if (verifiedUser) {
                  this.isAdmin = verifiedUser.isAdmin == 1 ? true : false;
                  this.userDetails = user;
                  this.route.queryParams
                    .subscribe(params => this.return = params['return']);
                  // console.log('may sala');
                  // if (this.return.indexOf("forums") > 0) 
                  //   this.router.navigate(['/view'])
                  // else
                  this.router.navigate([this.return])
                }

                else {
                  this.userDetails = null;

                  this._firebaseAuth.auth.signOut().then((res) => {
                    alert("You are not authorized to access this application!");
                    this.router.navigate(['/'])
                  });
                }
              } else {

                let verifiedUser = this.loginUserInfo.find(x => x.email === user.email);
                this.isAdmin = verifiedUser.isAdmin == 1 ? true : false;
                this.route.queryParams
                  .subscribe(params => this.return = params['return']);
                {
                  //console.log(this.return);
                  if (!this.isAdmin)
                    this._firebaseAuth.auth.signOut().then((res) => {
                      alert("You are not authorized to access this application!");
                      this.router.navigate(['/'])
                    });
                  if (this.return)
                    this.router.navigate([this.return])
                }
                // console.log(this.return);
                // if (this.return.indexOf("forums") > 0)
                //   this.router.navigate(['/view'])
                // else
                console.log(this.isAdmin);

              }
            }

            else {
              this.userDetails = null;
            }
          }
        );


      }
    );




  }

  ngOnInit() {


  }

  signInWithTwitter() {
    return this._firebaseAuth.auth.signInWithPopup(
      new firebase.auth.TwitterAuthProvider()
    )
  }
  signInWithFacebook() {
    return this._firebaseAuth.auth.signInWithPopup(
      new firebase.auth.FacebookAuthProvider()
    )
  }
  signInWithGoogle() {
    this.preLogin = true;


    return this._firebaseAuth.auth.signInWithPopup(
      new firebase.auth.GoogleAuthProvider()
    )
  }


  get isLoggedIn(): boolean {
    if (!this.userDetails) {
      return false;
    } else {
      return true;
    }

  }

  logout() {
    this._firebaseAuth.auth.signOut()
      .then((res) => this.router.navigate(['/']));
  }
}
