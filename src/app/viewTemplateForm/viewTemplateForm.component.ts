import { Component, ViewChild, AfterViewInit } from "@angular/core";
import { IItem } from "../items/item";
import { itemService } from "../items/item.service";
import { Router } from '@angular/router';
import { AuthService } from "../services/auth.service";
import { ModalComponent } from "../shared/modal/modal.component";

@Component({
    selector: 'view-template-form',
    templateUrl: './viewTemplateForm.component.html',
    providers: [itemService]
})
export class viewTemplateFormComponent {

    _listFilter: string;
    get listFilter(): string {
        return this._listFilter;
    }
    set listFilter(value: string) {
        this._listFilter = value;
        this.filteredItems = this.listFilter ? this.performFilter(this.listFilter) : this.items;
    }

    filteredItems: IItem[];
    items: IItem[] = [];
    errorMessage: string;
    showFilter: boolean = false;
    p: string;

    constructor(private itemService: itemService,
        private router: Router,
        private authService: AuthService,
        ) {


    }
    performFilter(filterBy: string): IItem[] {
        filterBy = filterBy.toLowerCase();
        return this.items.filter((item: IItem) =>
            (item.requestNo == null ? '' : item.requestNo).toLocaleLowerCase().indexOf(filterBy) !== -1);

    }
    @ViewChild(ModalComponent) child;
    ngOnInit(): void {
        this.loadData();
    }

    loadData(): void {
        this.child.showLoading = true;
        this.itemService.getItems().subscribe(
            items => {
                if (this.authService.isAdmin){
                    this.items = items;
                    this.filteredItems = this.items;
                    this.showFilter = true;
                }else{
                    this.items = items.filter(x => x.requisitionerName == this.authService.name);
                    this.filteredItems = items.filter(x => x.requisitionerName ==this.authService.name);
                }
                

                this.child.showLoading = false;
            },
            error => this.errorMessage = <any>error
        );
    }

    newItem(): void {
        this.router.navigate(['/entry'])
    }

    btnRoute(id: string): void {
        this.router.navigateByUrl('/disp/' + id);
    };

    btnRouteAccomplish(id: string): void {
        this.router.navigateByUrl('/entry/' + id + "/completion");
    };

    report(): void{
        this.router.navigate(['/report'])
    }

    
    performFilterByStatus(filterBy: string): void{
        this.itemService.getItems().subscribe(
            items => {
                this.items = items.filter(x=> x.status == filterBy);
                this.filteredItems = this.items;
             },
            error => this.errorMessage = <any>error
        ); 
    }




}