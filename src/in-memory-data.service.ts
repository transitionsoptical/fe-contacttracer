import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemDataService implements InMemoryDbService {
  createDb() {
    let users = [
      { id: "mjumarang@transitions.com", name: 'Jumarang, Mark' },
      { id: "coren.silva@essilor.com", name: 'Silva, Coren' },
      { id: "Jalliza.gomez@essilor.com", name: 'Gomez, Jalliza Eiva' }
      
    ];
    return {users: {
      total: users.length,
      results: users
    }};
  }
}


