// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAeJ7lgcP9CnN9VXi0KF6pcV7Y7YZVlCBY",
    authDomain: "transitions-auth.firebaseapp.com",
    databaseURL: "https://transitions-auth.firebaseio.com",
    projectId: "transitions-auth",
    storageBucket: "transitions-auth.appspot.com",
    messagingSenderId: "436857112405"
  },
  vendorAPI: 'https://insight-core.transitions.com/tos-vendor/api',
  mainAPI: 'https://insight-core.transitions.com/api',
  appID: '16'
};

